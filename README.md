---
title: Ma première expérience avec un dépôt de versioning - gitlab
author: Raphaëlle Krummeich
date: 2020/2021
tags: Git,lisez-moi,readme,monpremiergit
---
# Bienvenu-e sur mon Git
Je décris ici une prise en main en cours de Git en mode multi-compte sur un système linux, notamment grâce au [tutorial de Raghav Pal](https://youtu.be/lLgWWtOk7gk) qui publie des tuto "pas à pas" sur [sa chaîne youtube](https://www.youtube.com/channel/UCTt7pyY-o0eltq14glaG5dg).   
*09/04/2020*  
Je suis en phase de test pour l'installation d'un runner... épisode 6 du tutorial de Raghav Pal.   
et l'essai est réussi ! (my first job done)  
Belle journée !  
*07/10/2020*  
Ma bécane a pris la pluie (sous le hublot), elle est cramée... je recommence...rai plus tard !  
*06/01/2021*  
Après une bonne pause... je reprends  
*04/03/2022*  
La seconde pause est plus longue mais aussi plus productive... je reprends

# Donc, ce qui se passe avant...de tester un runner...[work in progress]  
A terme et en détail, vous trouverez ici les définitions données par Raghav Pal et les commandes exécutées...  
Comme les exemples qu'il donne sont tous dans l'un des deux environnements standard (OS Windows ou Mac), je procède à quelques aménagements de la leçon pour ubuntu.

# Le tutorial de Raghav Pal
## Connaître son système d'exploitation
Mon système d'exploitation est la dernière version d'Ubuntu, soit pour la connaître précisemment, j'utilise la commande :  
```
~$ lsb_release -a
```
ce qui me donne :
```
Distributor ID:	Ubuntu
Description:	Ubuntu 20.04.1 LTS
Release:	20.04
Codename:	focal
```
## Installer Git
Installer git en suivant les recommandations initiales proposées par @bcag2 qu'on trouve en français [ici](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git)   
J'ai opté pour une installation comme un paquet ce qui donne la commande suivante sur mon terminal (pour ouvrir un terminal **ctrl alt t**) :  
```
~$ sudo apt install git-all
```
puis, pour vérifier la version que je viens d'installer :  
```
~$ git --version
```
ce qui me donne :  
```
git version 2.25.1
```
## Apprendre Git
Les étapes à suivre pour apprendre à utiliser Git figurent sur [les premières vidéos de la chaîne de Raghav Pal](https://www.youtube.com/playlist?list=PLhW3qG5bs-L8OlICbNX9u4MZ3rAt5c5GG).  
Comme j'ai plusieurs comptes git, je choisi de ne pas configuer un compte en global, mais de suivre le tuto ci-dessous...[et avant, il est possible de lire les solutions proposées sur stackoverflow toujours actif depuis 10 ans](https://stackoverflow.com/questions/8801729/is-it-possible-to-have-different-git-configuration-for-different-projects/54125961#54125961)

## Configuration locale de plusieurs dépôts
Afin de préciser au sein de quel dépôt git je vais travailler, il est nécessaire de créer une clef ssh pour chaque compte/dépôt.  
La première étape est donc de se rendre dans le dossier dédié .ssh (fichier caché du répertoire home, que l'on peut voir avec la commande `~$ ls -a`):  
```
~$ cd .ssh
```
puis, de vérifier qu'il n'existe pas encore de clef en vérifiant le contenu du dossier avec la commande `~$ ls`.

### Créer des clefs ssh
Il s'agit ici de créer des clefs pour chaque compte git (personnel, professionnel etc.). Une même commande permet de générer chaque clef qui sera alors sauvegardée dans un fichier distinct dont j'aurai pris soin de désigner le nom.  Voilà pour la première clef :  
```!
ssh-keygen -t rsa -b 4096 -C "monadressecourriel@nomdedomaine.extension"
```
que je sauvegarde dans un premier fichier d'identité que je nomme `id_rsa_identite1`.  
De même, pour un second git (une seconde identité git), je créé une seconde clef ssh pour la courriel associé à cette identité que je sauvegarde dans un second fichier d'identité que je nomme `id_rsa_identite2`.  
Dans le dossier .ssh j'ai alors deux fichiers par identité, par exemple `id_rsa_identite1` et `id_rsa_identite1.pub`.

### Configurer les identités/utilisateurs localement
#### Créer le fichier config dans le dossier ~/.ssh

Toujours dans le dossier .ssh, je créée et édite le fichier de configuration des identités, nommé config, soit  
```
$ nano config
```
dans lequel je définis mes deux identités en précisant le git utilisé ainsi que la clef ssh associée :  
```
# Instance 1 - configuration par défaut
  Host identite1
  HostName gitlab.com
  User git
  IdentityFile ~/.ssh/id_rsa_identite1
# Instance 2
  Host identite2
  HostName github.com
  User git
  IdentityFile ~/.ssh/id_rsa_identite2
```

#### Créer le fichier .gitconfig à la racine ~./

Aller à la racine :  
`$ cd ~`  
créer et éditer le fichier .gitconfig :  
`$ nano .gitconfig`  
et préciser les identités locales :  
```
[user]
   name = nom prénom
# celle par défaut
   email = monadressecourriel@nomdedomaine.extension
[includeIf "gitdir:~/DossierGit_identite2/"]
# je pécise ici le dossier de travail de mon identité secondaire
   path = ~/DossierGit_identite2/.gitconfig
# et indique le chemin du fichier de configuration de la seconde identité
```
Puis aller dans le répertoire de travail secondaire :   
`$ cd DossierGit_identite2`  
et créer la seconde configuration d'identité en créant et éditant un fichier .gitconfig dans ce dossier :  
`$ nano .gitconfig`  
puis en renseignant la seconde identité :  
```
[user]
name = nom prénom
email = monadressecourriel@nomdedomaine.extension
# le cas échéant, un autre courriel que le précédent
```
C'est ce second fichier .gitconfig situé dans le dossier de travail secondaire `DossierGit_identite2` que le premier fichier .gitconfig situé à la racine appelle avec l'instruction `includeIf`.

#### Ajouter les identités ainsi créées

Je me rends dans le dossier .ssh :  
`$ cd ~/.ssh`  
Je supprime les éventuelles clefs d'identités ajoutées au préalable :  
`$ ssh-add -D`  
ce que précise le terminal :  
`all identities are removed`  
puis j'ajoute respectivement les identités créées :  
```
$ ssh-add id_rsa_identite1
$ ssh-add id_rsa_identite2
```
ce qui précise chaque identité ajoutée sous la forme :  
`Identity added: id_rsa_identite1 (monadressecourriel@nomdedomaine.extension)` *pour la première identité, par exemple*

#### Initier la connexion aux différents dépôts

Je me connecte au premier dépôt :  
`$ ssh -T identite1`  
et obtient en réponse :   
```
The authenticity of host (...) can't be establish.
ECDSA key (...) is SHA (...)
Are you sure you want ton continue connecting (yes/no/[fingerprint])?
```
et je réponds :  
`yes`  
et reçois une alerte simple :  
`Warning: Permanently added 'github.com,ip' (ECDSA) to the list of known hosts`   
et un message de bienvenue :  
`Welcome to Github, @monnomsurledepot !`  
Il se passe la même chose lorsque je me connecte au second dépôt avec la seconde identité.

#### Cloner chaque dépôt

Je me rends dans le dossier de travail pour ma première identité :  
`$ cd ~/DossierGit_identite1`  
Sur mon premier dépôt en ligne, je copie l'adresse du dépôt (en cliquant sur l'onglet clone) :  
![Image de l'interface avec l'adresse du dépôt à copier selon le protocole SSH](img/imgn.png)  
puis je clone mon dépôt, en modifiant la copie dans la ligne de commande pour préciser mon identité locale associée à ce dépôt, ci-dessous `identite1` :  
`$ git clone git@identite1:nomduproprietairedudepot/nomdudepot.git`  

# Utiliser Git

Un très bon guide de commandes de base réalisé par un collègue se trouve [ici](https://touticphoto.fr/developpement/14-git-pour-la-gestion-de-depot-de-codes-source-resume).

# Quelques tests & fonctionalités à connaître [*à venir*]
